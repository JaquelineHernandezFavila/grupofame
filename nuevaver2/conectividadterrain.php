<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>AUTOS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
         <link rel="stylesheet" type="text/css" href="css/image.css">

    </head>
    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>
       

                <?php include('menu.php'); ?>

        <div class="main-container">

 
            <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-8">
                            <h2>Obtener mas información.</h2>
                            
                            <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">
                                <div class="col-md-4">
                                    <input class="validate-required" type="text" name="NAME" placeholder="Tu Nombre" />
                                </div>
                                <div class="col-md-4">
                                    <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Correo Electrónico" />
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn--primary type--uppercase">Obtener Información</button>
                                </div>
                                <div class="col-md-12">
                                    <input class="validate-required" type="checkbox" name="group[13737][1]" />
                                    <span>Acepto
                                        <a href="#">terminos y condiciones</a>
                                    </span>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                    <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>}
                       
 </div>
           
           <br> 
           <h1 class="text-center">TECNOLOGÍA SIN COMPLICACIONES</h1>
            <p align="center">Mantente conectado dentro de tu GMC Terrain.</p> 
        <img class="img-responsive"  src="img/marcas/gmc/terraine2019/conectividadportadaterrain.jpg" width="1349" height="600" alt="Sin imagen">
        
        <div class="small-12 medium-12 xlarge-6  grid-column-alignment-left  columns">  
            <div class="row justify-content-around">
                <div class="medium-margin">
                    <div class="hover-bio-wrapper">
                        <div class="hover-bio">
                        <figure class="hover-bio">
                        <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria24.jpg" width="650" height="327" >
                        <figcaption>
                        <h2>Phone Projection</h2>
                        <p>Con Phone Projection accede a la información de tu smartphone con Apple CarPlay™1 y Android Auto™2.</p>
                        </figcaption>
                        </figure>
                        </div>
                        </div>

                <div class="hover-bio-wrapper">
                        <div class="hover-bio">
                        <figure class="hover-bio">
                        <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria25.jpg" width="650" height="327" >
                        <figcaption>
                        <h2>Sistema de infoentretenimiento de última generación con pantalla táctil de 8"</h2>
                        <p>Alta conectividad, comandos de voz, conexión Bluetooth y navegación. Toda la conectividad que necesitas en la punta de tus dedos.</p>
                        </figcaption>
                        </figure>
                        </div>
                        </div>
                </div>     
           
            <div class="row justify-content-around">
            <div class="medium-margin">            
                 <div class="hover-bio-wrapper">
                 <div class="hover-bio">
                 <figure class="hover-bio">    
                <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria26.jpg" width="650" height="327" alt="Sin imagen">
                <figcaption>
                <h2>Tráfico en tiempo real (Denali)</h2>
                <p>Haz que tus viajes sean impactantes y escápate del tráfico para siempre disfrutar del camino.</p>
                </figcaption>
                </figure>
                </div>
                </div>

            <div class="hover-bio-wrapper">
                    <div class="hover-bio">
                    <figure class="hover-bio">  
                    <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria27.jpg" width="650" height="327" alt="Sin imagen"> 
                    <figcaption>
                    <h2>OnStar® 4G LTE</h2>
                    <p>OnStar®† 4G LTE te permite conectar hasta 7 dispositivos a un rango de hasta 15 metros de distancia a un Hotspot de WiFi® con 3 meses ó 3GB de servicio gratis de internet (lo que ocurra primero).</p>
                    </figcaption>
                    </figure>
                    </div>
                    </div>
                 </div>
            </div>
          </div>
        </div> 
          

         

       
       
           </section>
            <div class="modal-container">
                <div class="modal-content">
                    <section class="imageblock feature-large bg--white border--round ">
                        <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                            <div class="background-image-holder">
                                <img alt="image" src="img/cowork-8.jpg" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-7">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-10">
                                            <h1>Ideal for design conscious startups.</h1>
                                            <p class="lead">
                                                Start building a beautiful site for your startup &mdash; right in the comfort of your browser.
                                            </p>
                                            <hr class="short">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input type="email" name="Email Address" placeholder="Email Address" />
                                                    </div>
                                                    <div class="col-12">
                                                        <input type="password" name="Password" placeholder="Password" />
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <span class="type--fine-print">By signing up, you agree to the
                                                            <a href="#">Terms of Service</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                    <!--end of row-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                    </section>
                </div>
            </div>
            <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="Image" class="logo" src="img/logo-dark.png" />

                             
                            <ul class="list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <span class="type--fine-print"><i class="fa fa-phone"> </i> 01 800 670 8386 </span>
                                    </a>
                                </li>
                                 
                            </ul>
                        </div>

                         <div class="col-md-5">
                            <span class="type--fine-print">&reg;
                                <span class="update-year"></span> FAME Manantiales.</span>
                            <a class="type--fine-print" href="#">Aviso de Privasidad </a>
                            <a class="type--fine-print" href="#">Formato ARCO</a>
                        </div>

                        <div class="col-md-3 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--end of row-->
                    
                    
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>