<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>GMC Manantiales FAME</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css'>

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/slider.css">
        
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="css/redesstyle.css">
  </head>


    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>

        <?php include('menu.php'); ?>
        <a id="start"></a>
        <div class="main-container">
            <section class="text-center cta cta-4 space--xxs border--bottom imagebg" data-gradient-bg='#2A3D86,#D9DADC,#231F20,#2A3D86'>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<span><font size="5"> Bienvenido a la Web Oficial de
                                <a href="#">Buick, GMC y Cadillac FAME Manantiales</a></font></span>
                                <br>
                                <br>
                                <br>
                                <br>-->

                                <div class="slider">
    <div class="slide-track">

            <img src="img/imgindex.jpg" height="100"  alt="" />


            <img src="img/imgindex.jpg" height="100"  alt="" />


            <img src="img/imgindex.jpg" height="100"  alt="" />


            <img src="img/imgindex.jpg" height="100"  alt="" />

    </div>
</div>

                                  <nav class="menu">
                                       <input type="checkbox" href="#" class="menu-open" name="menu-open" id="menu-open" />
                                       <label class="menu-open-button" for="menu-open">
                                        <span class="lines line-1"></span>
                                        <span class="lines line-2"></span>
                                        <span class="lines line-3"></span>
                                      </label>

                                       <a href="#" class="menu-item red"> <i class="fa fa-whatsapp"></i> </a>
                                       <a href="#" class="menu-item blue"> <i class="fa fa-facebook"></i> </a>
                                       <a href="#" class="menu-item blue"> <i class="fa fa-twitter"></i> </a>
                                       <a href="#" class="menu-item orange"> <i class="fa fa-phone"></i> </a>
                                       <a href="#" class="menu-item purple"> <i class="fa fa-instagram"></i> </a>
                                       <a href="#" class="menu-item lightblue"> <i class="fa fa-usd"></i> </a>
                                    </nav> 
       
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

            <!-- <section class="slider" data-arrows="true" data-paging="true">
                <ul class="slides">
                    <li>
                        <img alt="Image" src="img/1.jpg" />
                    </li>
                    <li>
                        <img alt="Image" src="img/2.jpg" />
                    </li>
                    <li>
                        <img alt="Image" src="img/3.jpg" />
                    </li>
                </ul>
            </section>

        -->
        
<div class="swiper-container main-slider loading">
  <div class="swiper-wrapper">
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria1.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria1.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Shaun Matthews</p>
        <span class="caption">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria2.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria2.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Alexis Berry</p>
        <span class="caption">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria3.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria3.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Billie Pierce</p>
        <span class="caption">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria4.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria4.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Trevor Copeland</p>
        <span class="caption">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria5.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria5.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Bernadette Newman</p>
        <span class="caption">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
      </div>
    </div>
  </div>
  <!--<div class="swiper-button-prev swiper-button-white"></div>
  <div class="swiper-button-next swiper-button-white"></div>-->
  <!-- If we need navigation buttons -->
</div>

<!-- Thumbnail navigation -->
<div class="swiper-container nav-slider loading">
  <div class="swiper-wrapper">
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria1.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria1.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Shaun Matthews</p>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria2.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria2.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Alexis Berry</p>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria3.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria3.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Billie Pierce</p>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria4.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria4.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Trevor Copeland</p>
      </div>
    </div>
    <div class="swiper-slide">
      <figure class="slide-bgimg" style="background-image:url(img/marcas/gmc/terraine2019/galeria5.jpg)">
        <img src="img/marcas/gmc/terraine2019/galeria5.jpg" class="entity-img" />
      </figure>
      <div class="content">
        <p class="title">Bernadette Newman</p>
      </div>
    </div>

  </div>
</div>

<div class="slider">
    <div class="slide-track">
        <div class="slide">
            <img src="img/logo-fame.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-gmc.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-cadillac.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-buick.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-fame.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-gmc.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-cadillac.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-buick.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
    </div>
</div>
            
    </div>


            
    </div>
            <div class="cajaform">
            <section class="space--xs2 imagebg" data-gradient-bg='#2A3D86,#D9DADC,#231F20,#2A3D86'>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                          <a href="formu.php">
                            <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                              <i class="icon icon--lg far fa-money-bill-alt"></i>  
                                <h4>Cotiza tu Auto</h4>
                            </div>
                            </a>
                            <!--end feature-->
                        </div>

                         <div class="col-md-3">
                          <a href="formu.php">
                            <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                <i class="icon icon--lg far fa-check-circle"></i>
                                <h4>Cita de Manejo</h4>
                            </div>
                            </a>
                            <!--end feature-->
                        </div>

                        <div class="col-md-3">
                          <a href="formu.php">
                            <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                <i class="icon icon--lg fas fa-cogs"></i>
                                <h4>Cita de Servicio</h4>
                                <span class="label">New</span>
                            </div>
                            </a>
                            <!--end feature-->
                        </div>
                        <div class="col-md-3">
                          <a href="formu.php">
                            <div class="feature feature-3 boxed boxed--lg boxed--border text-center">
                                <i class="icon icon--lg fas fa-wrench"></i>
                                <h4>Refacciones</h4>
                            </div>
                            </a>
                            <!--end feature-->
                        </div>
                    </div>
                    <!--end of row-->

                  
                </div>
                <!--end of container-->
            </section>
        </div>

        
            <div class="modal-container">
                <div class="modal-content">
                    <section class="imageblock feature-large bg--white border--round ">
                        <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                            <div class="background-image-holder">
                                <img alt="image" src="img/cowork-8.jpg" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-7">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-10">
                                            <h1>Ideal for design conscious startups.</h1>
                                            <p class="lead">
                                                Start building a beautiful site for your startup &mdash; right in the comfort of your browser.
                                            </p>
                                            <hr class="short">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input type="email" name="Email Address" placeholder="Email Address" />
                                                    </div>
                                                    <div class="col-12">
                                                        <input type="password" name="Password" placeholder="Password" />
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <span class="type--fine-print">By signing up, you agree to the
                                                            <a href="#">Terms of Service</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                    <!--end of row-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                    </section>
                </div>
            </div>
            
            <div class="cajainf">
            <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="Image" class="logo" src="img/logo-dark.png" />

                             
                            <ul class="list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <span class="type--fine-print"><i class="fa fa-phone"> </i> 01 800 670 8386 </span>
                                    </a>
                                </li>
                                 
                            </ul>
                        </div>

                         <div class="col-md-5">
                            <span class="type--fine-print">&reg;
                                <span class="update-year"></span> FAME Manantiales.</span>
                            <a class="type--fine-print" href="#">Aviso de Privacidad </a>
                            <a class="type--fine-print" href="#">Formato ARCO</a>
                        </div>

                        <div class="col-md-3 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--end of row-->
                    
                    
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
       <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/js/swiper.min.js'></script>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

       <script  src="js/index.js"></script>

    </body>
</html>