<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>AUTOS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="lightbox/css/lightbox.min.css">

    </head>
    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>
       

                <?php include('menu.php'); ?>

        <div class="main-container">

 <section class="text-center bg--secondary">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-12">
                            <h2><font color="teal"> Todo lo que necesitas esta aqui. </font></h2>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Clock-Back color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        <a href="formu.php"> <font color="#0000">Prueba de Manejo</font></a>
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Duplicate-Window color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        <a href="formu.php"> <font color="#0000">Cotizacion</font></a>
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Life-Jacket color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        <a href="formu.php"> <font color="#0000">Servicio</font></a>
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>                       
                        </div>
                    </div>
                    <!--end of row-->
                </div>  
                <!--end of container-->
            </section>

           <!-- <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-8">
                            <h2>Obtener mas información.</h2>
                            
                            <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">
                                <div class="col-md-4">
                                    <input class="validate-required" type="text" name="NAME" placeholder="Tu Nombre" />
                                </div>
                                <div class="col-md-4">
                                    <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Correo Electrónico" />
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn--primary type--uppercase">Obtener Información</button>
                                </div>
                                <div class="col-md-12">
                                    <input class="validate-required" type="checkbox" name="group[13737][1]" />
                                    <span>Acepto
                                        <a href="#">terminos y condiciones</a>
                                    </span>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                    <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                
                </div>
                
            </section>-->

            <section class="switchable">
                <div class="container">
                    <div class="row justify-content-around">
 
                        <div class="col-md-12 text-center">

                            <h1>Terrain 2019</h1>
                            <div class="lrge-margin">
                            <div>
                            <h2 class="q-headline1 text-center">
                            Tu nueva camioneta familiar
                            <br>
                            </h2>
                            </div>
 
                        </div>

                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
                <img alt="Image" src="img/marcas/gmc/terraine2019/terrainportada.jpg" width="2000" />
            
            <section class="switchable">
                <div class="container">
                    <div class="small-8 medium-8 xlarge-8  grid-column-alignment-left  columns">
                        <div class="q-margin-base q-headline">
                            <div class="medium-margin">
                                <div>
                                    <h2 class="q-display2 text-center">
                                    CARACTERÍSTICAS PRINCIPALES<br>
                                    </h2>
           
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-around">

                        <div class="col-md-12 text-center">
                            <a href="interiorterrain.php"><h3 class="q-headline3"> INTERIOR </h3></a>
                            <div align="left">
                            <img class="img-responsive" src="img/marcas/gmc/terraine2019/asientoterraine.jpg" width="650"  alt="Sin imagen" div align="right"><h3>Asientos de piel calefactables y ventilables (Denali)
                            Piel de calidad premium al interior de GMC Terrain 2019. </h3>
                             <a href="interiorterrain.php" class="btn btn-success">Mas Información</a>
                            
                            </div>
                        <div class="col-md-12 text-center">
                                <br> <br> <br>
                                <a href="exteriorterrain.php"><h3 class="q-headline3"> EXTERIOR </h3></a>
                            <div align="left">
                                <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria3.jpg" width="650" alt="Sin imagen" <div align="right"><h3>Rines de 19" en aluminio (Denali)
                                El estilo de GMC Terrain 2019 empieza desde los rines.</h3>
                                <a href="exteriorterrain.php" class="btn btn-success">Mas Información</a>

                                <div class="col-md-12 text-center">
                                    <br> <br> <br>
                                    <a href="conectividadterrain.php"><h3 class="q-headline3"> CONECTIVIDAD </h3></a>
                                        <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria12.jpg" width="650" alt="Sin imagen"<div align="right"><h3>Phone Projection 
                                        Con Phone Projection accede a la información de tu smartphone con Apple CarPlay™† y Android Auto™†.
                                        OnStar®† 4G LTE
                                        Hotspot WiFi® para hasta 7 dispositivos.
                                        </h3>
                                        <a href="conectividadterrain.php" class="btn btn-success">Mas Información</a>

                                <div class="col-md-12 text-center">
                                        <br> <br> <br> 
                                    <a href="desempeño.php"><h3 class="q-headline3"> RENDIMIENTO </h3></a>
                                     <img class="img-responsive"  src="img/marcas/gmc/terraine2019/galeria14.jpg" width="650" height="300" alt="Sin imagen" <div align="right"><h3>Motor 2.0 lt. 
                                    Desempeño y respuesta premium con transmisión de 9 velocidades.
                                    <a href="desempeño.php" class="btn btn-success">Mas Información</a>

                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
             <section class="switchable">
                <div class="container">
                <div class="q-margin-base  q-separator">
                   
                        <hr class="invisible">
                    </div>
                <div class="q-margin-base q-headline">
                <div class="medium-margin     ">
                    
                    <div>
                    <h2 class="q-display2 ">
                        
                                GALERIA<br>
                        
                    </h2>    
                    </div>
                </div>
            </div>
            <div class="container">
         <articles class="row">
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria1.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria1.jpg" alt="Regala" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria2.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria2.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria3.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria3.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria4.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria4.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria5.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria5.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria6.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria6.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria7.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria7.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria8.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria8.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria9.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria9.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria10.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria10.jpg" alt="Descansa" class="img-thumbnail"></a>
                
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria11.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria11.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria12.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria12.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria13.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria13.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria14.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria14.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria15.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria15.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria16.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria16.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria17.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria17.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
            <article class="col-md-3">
                <a href="img/marcas/gmc/terraine2019/galeria18.jpg" data-lightbox="example-set">
                <img src="img/marcas/gmc/terraine2019/galeria18.jpg" alt="Descansa" class="img-thumbnail"></a>
            </article>
                 </articles>

                 <div class="small-12 medium-12 xlarge-12  grid-column-alignment-left  columns">
            <div class="q-margin-base q-headline">
                    <div class="medium-margin     ">     
                        <div>
                        <h2 class="q-display2 ">
                            
                                    ESPECIFICACIONES PRINCIPALES<br>
                            
                        </h2>
                        </div>
                    </div>
                </div>
                <div class="q-margin-base q-image-svg">
                    <div class="none-margin     "> 
                        <div class="image-svg-container">
                        <img class="img-responsive"  src="img/marcas/gmc/2019-encore-mov-specs-01.jpg" >
                    </div>  
                </div>
            </div>
                
            </div>
        </div>
    </div>
</section>
            <section class="text-center bg--secondary">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-12">
                            <h2><font color="teal">Todo lo que necesitas esta aqui.</font></h2>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Clock-Back color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        <a href="formu.php"> <font color="#0000">Prueba de Manejo</font></a>
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Duplicate-Window color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        <a href="formu.php"> <font color="#0000">Cotizacion</font></a>
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Life-Jacket color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        <a href="formu.php"> <font color="#0000">Servicio</font></a>
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        
                       
                         
                        </div>
                    </div>
                    <!--end of row-->
                </div>  
                <!--end of container-->
            </section>
            <section class="switchable switchable--switch feature-large bg--primary">
                <div class="container">
                    <div class="row justify-content-around">

                        <div class="col-md-3 col-lg-3">
                            
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="video-cover border--round box-shadow-wide">
                                <div class="background-image-holder">
                                    <img alt="image" src="img/landing-8.jpg" />
                                </div>
                                <div class="video-play-icon"></div>
                                <iframe data-src="https://www.youtube.com/embed/fc57JMwUOUI?autoplay=1" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                            <!--end video cover-->
                        </div>
                        <div class="col-md-3 col-lg-3">
                            
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                       <div class="col-md-10 col-lg-8">
                            <h2>Obtener mas información.</h2>
                            
                            <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">
                                <div class="col-md-4">
                                    <input class="validate-required" type="text" name="NAME" placeholder="Tu Nombre" />
                                </div>
                                <div class="col-md-4">
                                    <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Correo Electrónico" />
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn--primary type--uppercase">Obtener Información</button>
                                </div>
                                <div class="col-md-12">
                                    <input class="validate-required" type="checkbox" name="group[13737][1]" />
                                    <span>Acepto
                                        <a href="#">terminos y condiciones</a>
                                    </span>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                    <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            

           
 </div>
           
             
           
            <div class="modal-container">
                <div class="modal-content">
                    <section class="imageblock feature-large bg--white border--round ">
                        <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                            <div class="background-image-holder">
                                <img alt="image" src="img/cowork-8.jpg" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-7">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-10">
                                            <h1>Ideal for design conscious startups.</h1>
                                            <p class="lead">
                                                Start building a beautiful site for your startup &mdash; right in the comfort of your browser.
                                            </p>
                                            <hr class="short">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input type="email" name="Email Address" placeholder="Email Address" />
                                                    </div>
                                                    <div class="col-12">
                                                        <input type="password" name="Password" placeholder="Password" />
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <span class="type--fine-print">By signing up, you agree to the
                                                            <a href="#">Terms of Service</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                    <!--end of row-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                    </section>
                </div>
            </div>
            <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="Image" class="logo" src="img/logo-dark.png" />

                             
                            <ul class="list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <span class="type--fine-print"><i class="fa fa-phone"> </i> 01 800 670 8386 </span>
                                    </a>
                                </li>
                                 
                            </ul>
                        </div>

                         <div class="col-md-5">
                            <span class="type--fine-print">&reg;
                                <span class="update-year"></span> FAME Manantiales.</span>
                            <a class="type--fine-print" href="#">Aviso de PrivaCidad </a>
                            <a class="type--fine-print" href="#">Formato ARCO</a>
                        </div>

                        <div class="col-md-3 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--end of row-->
                    
                    
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="lightbox/js/lightbox.min.js"></script>
  <script>
      lightbox.option({
        'albumLabel': "Imagen %1 de %2"
      })
  </script>
    </body>
</html>