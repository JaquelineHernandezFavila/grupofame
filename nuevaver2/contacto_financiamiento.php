<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

<head>


</head>

<body>
        <div class="main-container">

 
             <section class=" bg--secondary">
                <div class="container">

                    <div class="row justify-content-center">

                        <div class="col-md-4 col-lg-4">
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div style="font-size: 28px; " >

                                    <button href="https://api.whatsapp.com/send?phone=524433250949&text=Hola,%20Quiero%20más%20información!" class="btn btn-success">
                                        
                                    &nbsp; <i style="font-size: 32px; color:white;" class="fab fa-whatsapp"></i>   WhatsApp &nbsp; <br>
                                                
                                    </button>
                                </div>
                            </div>
                            <!--end of row-->
                        </div>

                    </div>

                    <br>


                    <div class="row justify-content-center">

                        

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="boxed boxed--border">
                                    <form class="text-left form-email row" data-success="Gracias por contactarnos, en breve un asesor te contactará." data-error="Por favor ingresa los datos correctos." >
                                        <div class="col-md-6">
                                            <span>Nombre:</span>
                                            <input type="text" name="name" class="validate-required" />
                                        </div>
                                        <div class="col-md-6">
                                            <span>Ciudad:</span>
                                            <input type="text" name="company" class="validate-required" />
                                        </div>
                                        <div class="col-md-6">
                                            <span>Correo:</span>
                                            <input type="email" name="email" class="validate-required validate-email" />
                                        </div>
                                        <div class="col-md-6">
                                            <span>Telefono:</span>
                                            <input type="tel" name="phone" class="validate-required" />
                                        </div>
                                        <div class="col-md-12">
                                            <span>Comentarios:</span>
                                            <textarea rows="3" name="description" class="validate-required"></textarea>
                                        </div>
                                        <div class="col-md-12 text-center boxed">
                                            <h5>Servicios requeridos</h5>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">1</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="design" />
                                                <label></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">2</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="dev" />
                                                <label></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">3</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="brand" />
                                                <label></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">4</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="marketing" />
                                                <label></label>
                                            </div>
                                        </div>
                                         
                                       
                                         
                                        <div class="col-md-12 boxed">
                                            <button type="submit" class="btn btn--primary type--uppercase">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end of row-->
                        </div>

                        <div class="col-md-1 col-lg-1">
                        </div>


                        <div class="col-md-5 col-lg-5">
                            <div class="row">
                                <div class="boxed boxed--border ">
                                    <form class="text-left form-email row" data-recaptcha-theme="light">
                                        <div class="col-md-12">
                                            

                                            <h3>Información de Contacto</h3>

                             

                            <p>
                                <span class="block"><strong><i class="fa fa-map-marker"></i> Dirección:</strong> Periférico Paseo de la República #2655 Col. Las Camelinas  Morelia, Michoacán.</span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Tel:</strong> <a href="tel:(443) 334 4440">(443) 334 4440</a></span>
                                <span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">contacto@famemanantiales.com</a></span>
                            </p>

                            <hr />

                            <p>

                                <span class="block"><strong><i class="fa fa-phone"></i> Recepción:</strong> <a href="tel:(443) 334 4440">Ext. 0</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Taller de Servicio:</strong> <a href="tel:(443) 334 4440">Ext. 100</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Postventa:</strong> <a href="tel:(443) 334 4440">Ext. 101</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Refacciones:</strong> <a href="tel:(443) 334 4440">Ext. 300</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Ventas:</strong> <a href="tel:(443) 334 4440">Ext. 200</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Seminuevos:</strong> <a href="tel:(443) 334 4440">Ext. 203</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Financiamiento:</strong> <a href="tel:(443) 334 4440">Ext. 204</a></span>
                                
                            </p>

                                <hr />

                            <h3>WhatsApp</h3>

                        

                            <p>
                                <span class="block"><strong><i class="fa fa-phone"></i> </strong> <a href="tel:4432279923">4432279923</a></span>
                                 
                                
                            </p>

                                            <h3>Horario de Atención</h3>

                            <p>
                                Con gusto esperamos tu llamada en nuestro Call Center, para cualquier duda, aclaración o sugerencia que quieras compartirnos en FAME Manantiales; te escuchamos y atendemos de manera personalizada.
                            </p>

                            <hr />
 

                            <h4 class="font300">Ventas</h4>
                            <p>
                                <span class="block"><strong>Lunes - Viernes:</strong> 09:00 - 19:30 hrs.</span>
                                <span class="block"><strong>Sábado:</strong> 09:00 - 17:00 hrs.</span>
                                <span class="block"><strong>Domingo:</strong> 11:00 - 15:00 hrs.</span>
                            </p>

                            <h4 class="font300">Servicio y Administración</h4>
                            <p>
                                <span class="block"><strong>Lunes - Viernes:</strong> 09:00 - 19:30 hrs.</span>
                                <span class="block"><strong>Sábado:</strong> 09:00 - 14:00 hrs.</span>
                                <span class="block"><strong>Domingo:</strong> Cerrado</span>
                            </p>


                                        </div>
                                         
                                        
                                         
                                    </form>
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
 

                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>


</body>

</html>