
<?php 
session_start() 
?>
<!Doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>GMC Manantiales FAME</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jaqui.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css'>

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/slider.css">        
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="css/redesstyle.css">

        <style type="text/css">
      input:invalid {
      background-color: #E0E2E3;  
      border-top: double;
      border-left: double;
      }

      select:invalid{
        background-color: #E0E2E3;
        border: double;
      }
  </style>

       
         </head>

<body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>

        <?php include('menu.php'); ?>
        <a id="start"></a>
        
        <a class="boton_personalizado"href="javascript:history.go(-1)">Atrás</a>
        
         <form action="validar.php" method="post" name="formulario">


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <legend class="text-center header">Cotización</legend>

                        <center><img class="imagen_formulario" src="img/formulario.png" width="400" height="400"></center> <br>
                        <!----><br><br><br>

<div class="slider">
    <div class="slide-track">
        <div class="slide">
            <img src="img/logo-fame.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-gmc.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-cadillac.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-buick.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-fame.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-gmc.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-cadillac.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-buick.jpg" height="100" width="250" alt="" />
        </div>
        <div class="slide">
            <img src="img/logo-agency.png" height="100" width="250" alt="" />
        </div>
    </div>
</div>
      <br><br>          

                        <form class="form-inline">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="fname" name="nombre" type="text" placeholder="Nombre Completo" class="form-control"
                                maxlength="30" size="30" pattern="([a-zA-ZÁÉÍÓÚñáéíóú]{1,}[\s]*)+" required>
                                 <script type="text/javascript"> var f3 = new LiveValidation('f3'); f3.add(Validate.Presence, {failureMessage: "Haz fallado, escribe tu nombre por favor!!! "}); </script> 

                            </div>
                        </div>
                       
                         <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="email" id="email" name="email" type="text" placeholder="Correo Electronico" class="form-control" required>
                                <script type="text/javascript"> var f4 = new LiveValidation('f4'); f4.add(Validate.Presence, {failureMessage: "Ocurrio un error con tu correo intentalo de nuevo!!!"}); </script>
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                            <div class="col-md-8">
                                <input type="text" id="telefono" name="telefono" type="text" placeholder="Numero de Telefono" class="form-control" pattern="[0-9]{10}" required>
                            <script type="text/javascript"> var f4 = new LiveValidation('f4'); f4.add(Validate.Presence, {failureMessage: "Por favor ingresa tu numero de telofono!!!"}); </script>
                                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-cog bigicon"></i></span>
                            <div class="col-md-8">
                                
                                 <select name="modelo" class="control-label" name="modelo" id="modelo" class="form-control selectpicker" required>
                                 <option value="" > Selecciona un Modelo</option><br><br><br>
                                 <option name="m1" id="Beat">Acadia</option>
                                 <option name="m2" id="Cavalier">CTS</option>
                                 <option name="m3" id="Spark">ATS</option>
                                 <option name="m4" id="Beat NB">CTS-V</option>
                                 <option name="m4" id="Equinox">ATS-V</option>
                                 <option name="m4" id="Aveo">Enclave</option>
                                 <option name="m4" id="Cheyenne">Encore</option>
                                 <option name="m4" id="Colorado">Envision</option>
                                 <option name="m4" id="Cruze">Escalade</option>
                                 <option name="m4" id="Suburban">Sierra</option>
                                 <option name="m4" id="Tahoe">Sierra All Terrain</option>
                                 <option name="m4" id="Tornado">Terrain</option>
                                 <option name="m4" id="Trax">XT5</option>
                                 <option name="m4" id="Malibu">Yukon</option>

                     </select>
                            </div>
                        </div>
                        <br>
                         <!-- Select SERVICIO -->
                         <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-cog bigicon"></i></span>
                            <div class="col-md-8">
                                
                                <select name="servicio" class="control-label" name="servicio" id="servicio" class="form-control selectpicker" required="" >
                                         <option value="" >Selecciona una Opción</option>
                                         <option name="s1" id="s1">Cotización de Vehiculo</option>
                                         <option name="s2" id="s2">Prueba de Manejo GRATIS</opt ion>
                                         <option name="s3" id="s3">Cita de Servicio</option>
                                         <option name="s4" id="s4">Cotizar Refacciones</option>
                                     </select>
                                 </div>
                             </div>
                         </div>
                          <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-8">
                        <div class="g-recaptcha" data-sitekey="6Lfd7o8UAAAAADl72v-17qasvLQY2grJBa-IXFv6"></div>
                    </div>
                </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                            </div>
                        </div>
                    
                </form>
                <script src = "https://www.google.com/recaptcha/api.js?render= reCAPTCHA_site_key " > </script> 
        <script>   grecaptcha . ready ( function () {       grecaptcha . execute ( ' reCAPTCHA_site_key ' , { action : ' homepage ' }). then ( function ( token ) { ... }); }); </script>
        <!--<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="misfunciones.js"></script>
            </div>
        </div>
    </div>
</div>
</body>
</html>
